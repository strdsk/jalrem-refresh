<?php

return [
    "title" => "Spice Route as World Heritage",
    "slide-0" => "Several in-depth conversations took place regarding the significance of the Spice Route from both Indonesian and international viewpoints. The group meeting centered on deliberating the definition and range of the Spice Route nomination, considering the potential of a collaborative submission with other nations.",

    "slide-1" =>    "Carrying out various intensive discussions about the Spice Route in various regions through online meetings.

                    <br/><br/>Working with ANRI to identify various VOC documents related to locations, commodities and spice trade policies.

                    <br/><br/>Cultural Caravan of the Spice Route tracing the traces of the spice trade in Sorong, Banda Naira, Selayar Islands, Makassar and Bali aboard Arka Kinari.",

    "slide-2" =>    "Conducting research on archeological objects related to the Spice Route carried out in various provinces in Indonesia.

                    <br/><br/>Carrying out the \"Festival Bumi Rempah Nusantara untuk Dunia\" (Nusantara Spices for the World) in 13 regions, including Banda Neira, North Maluku, Makassar, Banjarmasin, North Sumatra (Belawan), Kep. Riau (Bintan Island), Aceh (Lhoksumawe).",

    "slide-3" =>    "Carrying out the Spice Route Cultural Gathering Event in collaboration with the Indonesian Navy - KRI Dewaruci and involving 149 selected young people from 34 provinces in Indonesia called Laskar Rempah to explore the spice route points of the archipelago in Surabaya, Makassar, Baubau & Buton, Ternate & Tidore, Banda Naira , and Kupang. This activity seeks to reaffirm the identity of people across the archipelago as Indonesian and cultural Gathering connection through the warmth of spices.",

    "slide-4" =>    "Inclusion of the Spice route as a new Tentative List Candidate.

                    <br/><br/>Carrying out another Spice Route Cultural Gathering Event, in collaboration with the Indonesian Navy - KRI Dewaruci. This time, involving 50 participants consisting of people of young generation, researchers, social media activists and media to explore the Spice Route points",

    "slide-5" =>    "Submission of Spice Route as World Heritage.",

    "slide-0-acc" => "Discussion",
    "slide-1-acc" => "Discussion & Identification",
    "slide-2-acc" => "Research & Festival",
    "slide-3-acc" => "Cultural Gathering",
    "slide-4-acc" => "Inclusion & Cultural Gathering",
    "slide-5-acc" => "Submission",
];
