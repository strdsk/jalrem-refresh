<?php

return [
    "title" => "Jalur Rempah Menuju Warisan Dunia",

    "slide-0" =>    "Berbagai diskusi intensif mengenai peran Jalur Rempah dari sudut pandang Indonesia dan luar negeri. Pertemuan kelompok terfokus pada pembahasan definisi dan ruang lingkup nominasi Jalur Rempah, termasuk kemungkinan untuk mengajukan nominasi bersama dengan negara lain.",

    "slide-1" =>    "Melaksanakan berbagai diskusi intensif tentang Jalur Rempah di berbagai daerah secara daring.

                    <br/><br/>Bekerja sama dengan ANRI untuk mengidentifikasi berbagai dokumen VOC yang terkait dengan lokasi, komoditas, dan kebijakan perdagangan rempah.

                    <br/><br/>Karavan Budaya Jalur Rempah dengan menggunakan Kapal Arka Kinari yang menelusuri jejak perdagangan rempah di Sorong, Banda Naira, Kepulauan Selayar, Makassar, dan Bali.",

    "slide-2" =>    "Melakukan penelitian terhadap objek-objek terkait Jalur Rempah yang dilaksakan di berbagai provinsi di Indonesia.

                    <br/><br/>Melaksanakan Festival Bumi Rempah Nusantara untuk Dunia di 13 daerah, di antaranya Banda Neira, Maluku Utara, Makassar, Banjarmasin, Sumatera Utara (Belawan), Kep. Riau (Pulau Bintan), Aceh (Lhoksumawe).",

    "slide-3" =>    "Melaksanakan Muhibah Budaya Jalur Rempah bekerja sama dengan TNI AL - KRI Dewaruci dan melibatkan 149 pemuda-pemudi terpilih dari 34 provinsi di Indonesia yang disebut Laskar Rempah untuk menelusuri titik jalur rempah Nusantara di Surabaya, Makassar, Baubau & Buton, Ternate & Tidore, Banda Naira, dan Kupang. Kegiatan ini berupaya untuk menegaskan kembali keindonesiaan dan ketersambungan budaya melalui kehangatan rempah.",

    "slide-4" =>    "Pencantuman jalur Rempah sebagai Calon Daftar Sementara (Tentative List) baru.

                    <br/><br/>Melaksanakan kembali Muhibah Budaya Jalur Rempah dan bekerja sama dengan TNI AL - KRI Dewaruci dengan melibatkan 50 peserta yang terdiri dari pemuda-pemudi, peneliti, pegiat media sosial, dan media untuk menelusuri titik Jalur Rempah",

    "slide-5" =>    "Pengajuan Jalur Rempah sebagai Warisan Budaya Dunia.",

    "slide-0-acc" => "Diskusi",
    "slide-1-acc" => "Diskusi & Identifikasi",
    "slide-2-acc" => "Penelitian & Festival",
    "slide-3-acc" => "Muhibah Budaya",
    "slide-4-acc" => "Pencantuman & Muhibah Budaya",
    "slide-5-acc" => "Pengajuan",
];
